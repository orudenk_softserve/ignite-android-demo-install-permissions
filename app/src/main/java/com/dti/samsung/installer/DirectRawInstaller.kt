package com.dti.samsung.installer

import android.Manifest
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInstaller
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresPermission
import com.dti.samsung.MainActivity
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

class DirectRawInstaller(private val context: Context) : RawInstaller {

    @RequiresPermission(Manifest.permission.INSTALL_PACKAGES)
    override fun install(rawId: Int) {
        try {
            val inputStream: InputStream = context.resources.openRawResource(rawId)
            installPackage(context, inputStream, "com.dti.apkfile", true)
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    @Throws(IOException::class)
    fun installPackage(
        context: Context,
        inputStream: InputStream,
        packageName: String,
        isSilent: Boolean
    ): Boolean {
        val packageInstaller: PackageInstaller = context.packageManager.packageInstaller
        val params = PackageInstaller.SessionParams(
            PackageInstaller.SessionParams.MODE_FULL_INSTALL
        )
        params.setAppPackageName(packageName)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            if (isSilent) {
                params.setRequireUserAction(PackageInstaller.SessionParams.USER_ACTION_NOT_REQUIRED)
            }
        }
        // set params
        val sessionId = packageInstaller.createSession(params)
        val session = packageInstaller.openSession(sessionId)
        val out: OutputStream = session.openWrite("COSU", 0, -1)
        val buffer = ByteArray(65536)
        var c: Int
        while (inputStream.read(buffer).also { c = it } != -1) {
            out.write(buffer, 0, c)
        }
        session.fsync(out)
        inputStream.close()
        out.close()
        val intent = Intent(context, MainActivity.InstallingBroadcast::class.java)
        intent.putExtra(
            MainActivity.InstallingBroadcast.MESSAGE_KEY,
            packageName
        ) // TODO add more data
        val i = PendingIntent.getBroadcast(
            context,
            REQUEST_CODE_BROADCAST,
            intent,
            PendingIntent.FLAG_IMMUTABLE
        )
        session.commit(i.intentSender)
        return true
    }

    companion object {
        private const val TAG = "DirectRawInstaller:"
        const val REQUEST_CODE_BROADCAST = MainActivity.REQUEST_CODE_BROADCAST
    }
}