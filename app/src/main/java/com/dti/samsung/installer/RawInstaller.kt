package com.dti.samsung.installer

import androidx.annotation.RawRes

interface RawInstaller {

    // it is just a sample without metadata, with hardcoded package...
    fun install(@RawRes rawId: Int)
}