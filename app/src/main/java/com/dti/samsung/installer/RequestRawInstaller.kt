package com.dti.samsung.installer

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.content.FileProvider
import com.dti.samsung.MainActivity
import com.dti.samsung.helpers.FileHelper
import java.lang.Exception

class RequestRawInstaller(
    private val context: Context,
    private val activityForResult: Activity
) : RawInstaller {

    override fun install(rawId: Int) {
        val apkURI = FileProvider.getUriForFile(
            context,
            context.packageName + ".provider",
            FileHelper.getCachedRawFile(rawId, context)
        )
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.setDataAndType(
                apkURI,
                "application/vnd.android.package-archive"
            )
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            activityForResult.startActivityForResult(intent, REQUEST_CODE_INSTALL)
        } catch (e: Exception) {
            Log.e(TAG, e.message, e)
        }
    }

    companion object {
        private const val TAG = "DirectRawInstaller:"
        private const val REQUEST_CODE_INSTALL = MainActivity.REQUEST_CODE_INSTALL
    }
}