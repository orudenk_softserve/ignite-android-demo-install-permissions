package com.dti.samsung

import android.Manifest.permission.INSTALL_PACKAGES
import android.Manifest.permission.REQUEST_INSTALL_PACKAGES
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioGroup
import androidx.annotation.RawRes
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.Context
import android.content.IntentFilter
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import com.dti.samsung.installer.DirectRawInstaller
import com.dti.samsung.installer.RawInstaller
import com.dti.samsung.installer.RequestRawInstaller


class MainActivity : AppCompatActivity() {

    private val installingBroadcast = InstallingBroadcast()
    private val directRawInstaller: RawInstaller = DirectRawInstaller(applicationContext)
    private val requestRawInstaller: RawInstaller = RequestRawInstaller(applicationContext, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        INSTALL_PACKAGES
        REQUEST_INSTALL_PACKAGES
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.install_action_btn).setOnClickListener {
            installDirect(getRawBuildRes())
        }
        findViewById<Button>(R.id.install_request_action_btn).setOnClickListener {
            installRequest()
        }
        findViewById<Button>(R.id.update_action_btn).setOnClickListener {
            //TODO update request for RAW
        }

        initBroadcast()
    }

    private fun initBroadcast() {
        val filter = IntentFilter()
        registerReceiver(installingBroadcast, filter)
    }

    @RawRes
    private fun getRawBuildRes(): Int {
        return when (findViewById<RadioGroup>(R.id.rg_build_number).checkedRadioButtonId) {
            R.id.rb_build_1 -> R.raw.com_dti_apkfile_1
            R.id.rb_build_2 -> R.raw.com_dti_apkfile_2
            R.id.rb_build_3 -> R.raw.com_dti_apkfile_3
            else -> R.raw.com_dti_apkfile_1
        }
    }

    private fun installDirect(@RawRes rawId: Int) {
        directRawInstaller.install(rawId)
    }

    private fun installRequest() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!packageManager.canRequestPackageInstalls()) {
                startActivityForResult( //TODO start with androidx
                    Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES).setData(
                        Uri.parse("package:$packageName")
                    ), REQUEST_CODE_PERMISSION
                )
            } else {
                doInstallRequestByIntent()
            }
        } else {
            doInstallRequestByIntent()
        }
    }

    private fun doInstallRequestByIntent() {
        val rawId = getRawBuildRes()
        requestRawInstaller.install(rawId)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult: requestCode=$requestCode, resultCode=$resultCode, data=$data")
        if (requestCode == REQUEST_CODE_PERMISSION && resultCode == Activity.RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && packageManager.canRequestPackageInstalls()) {
                doInstallRequestByIntent()
            }
        }
    }

    internal class InstallingBroadcast : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val message = intent?.getStringExtra(MESSAGE_KEY) ?: return
            Log.d(TAG, "InstallingBroadcast: message=$message")
            Log.d(TAG, "InstallingBroadcast: intent=$intent")
        }

        companion object {
            const val MESSAGE_KEY = "message"
        }
    }

    companion object {
        private const val TAG = "MainActivity"
        const val REQUEST_CODE_PERMISSION = 101
        const val REQUEST_CODE_INSTALL = 102
        const val REQUEST_CODE_BROADCAST = 201
    }

}