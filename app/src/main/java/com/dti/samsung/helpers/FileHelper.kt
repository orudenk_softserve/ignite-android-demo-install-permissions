package com.dti.samsung.helpers

import android.content.Context
import android.util.Log
import androidx.annotation.RawRes
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream


object FileHelper {
    private const val TAG = "FileHelper:"

    fun getCachedRawFile(@RawRes rawId: Int, context: Context): File {
        val cacheDir = context.cacheDir
        val tempFile = File.createTempFile("apk_", ".apk", cacheDir)
        try {
            val inputStream: InputStream = context.resources.openRawResource(rawId)
            val fileOutputStream = FileOutputStream(tempFile)
            val buf = ByteArray(1024)
            var len: Int
            while (inputStream.read(buf).also { len = it } > 0) {
                fileOutputStream.write(buf, 0, len)
            }
            fileOutputStream.close()
            inputStream.close()
        } catch (ioException: IOException) {
            Log.e(TAG, ioException.message, ioException)
        }
        Log.e(TAG, "tempFile file name is ${tempFile.name}")

        return tempFile
    }
}